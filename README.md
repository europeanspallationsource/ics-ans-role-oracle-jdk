ics-ans-role-oracle-jdk
=======================

Ansible role to install oracle JDK.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
oracle_jdk_version: "1.8.0_161"
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-oracle-jdk
```

License
-------

BSD 2-clause
