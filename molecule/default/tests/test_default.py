import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('default_group')


JAVA_VERSION = '1.8.0_161'


def test_java_version(host):
    cmd = host.command('java -version 2>&1')
    assert 'java version "{}"'.format(JAVA_VERSION) in cmd.stdout


def test_java_home(host):
    cmd = host.command('su -l -c "echo \$JAVA_HOME"')
    assert cmd.stdout.strip() == '/usr/java/jdk{}'.format(JAVA_VERSION)


def test_jce_policy(host):
    cmd = host.command('jrunscript -e \'exit (javax.crypto.Cipher.getMaxAllowedKeyLength("RC5") >= 256);\'')
    # The previous command returns 1, if the Unlimited Cryptography is available, 0 otherwise
    assert cmd.rc == 1


def test_secure_random_source(host):
    java_security = host.file('/usr/java/jdk{}/jre/lib/security/java.security'.format(JAVA_VERSION))
    assert not java_security.contains('securerandom.source=file:/dev/random')
    assert java_security.contains('securerandom.source=file:/dev/urandom')
