import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('previous_version')


JAVA_VERSION = '1.8.0_151'


def test_java_version(host):
    cmd = host.command('java -version 2>&1')
    assert 'java version "{}"'.format(JAVA_VERSION) in cmd.stdout
